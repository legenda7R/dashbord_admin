import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
user:any;
  
  
@Input() variableChildren:any;
  
  
@Output() nomEvent = new EventEmitter();
  
  
  constructor(private router:Router) { }
  
  ngOnInit(): void {
    console.log("user without parse :",localStorage.getItem("userConnecte"));
    console.log("user with parse :",JSON.parse(localStorage.getItem("userConnecte")));
    this.user = JSON.parse(localStorage.getItem("userConnecte"))
  }
logout(){
  //localeStorage.removeItem("state");
  //localeStorage.setItem('state','0')
  localStorage.clear();
  this.router.navigateByUrl('/')
  
}

nomFonction() {
  this.nomEvent.emit();
}
}
