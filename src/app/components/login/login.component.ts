import { Router } from '@angular/router';
import { ClientService } from './../../_service/client.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin:FormGroup;
  constructor( private fb:FormBuilder,private clientService:ClientService,private router:Router) { }
  ngOnInit(): void {
  this.geneFormLogin();
  }
  geneFormLogin(){
    this.formLogin = this.fb.group({
      email:'',
      password:''
    })
  }
  login(){
    console.log("here login",this.formLogin.value);
    this.clientService.login(this.formLogin.value).subscribe(
      (res:any) => {
        console.log("without stringfy client is : ",res);
        console.log(" with stringfy client is :",JSON.stringify(res));
        if(res != null){
          localStorage.setItem("state","1")
          localStorage.setItem("userConnecte",JSON.stringify(res))
          this.router.navigateByUrl("home")
          } else {
            this.router.navigateByUrl("home")
          }
      }
    )
  }
}