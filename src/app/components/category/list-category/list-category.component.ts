import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/_service/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {

  categorie:any
  constructor(private CategoryService:CategoryService) { }

  ngOnInit(): void {
    this.getallcategory();
  }
getallcategory(){
  this.CategoryService.getALLcategory().subscribe((res:any) =>{
    console.log("qlkc")
    this.categorie=res;
  })
}
delete(id_categorie){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.CategoryService.deletecat(id_categorie).subscribe((res:any) =>{
           console.log("client supprimer",res);
          this.getallcategory();
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
    }
  })

}
}

