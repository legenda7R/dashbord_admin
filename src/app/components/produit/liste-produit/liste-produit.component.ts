import { ProduitService } from './../../../_service/produit.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-liste-produit',
  templateUrl: './liste-produit.component.html',
  styleUrls: ['./liste-produit.component.css']
})
export class ListeProduitComponent implements OnInit {
  Produit :any;
  constructor(private ProduitService:ProduitService) { }

  ngOnInit(): void {
    console.log("here is ngOnInit ------")
    this.getAllProduct();
  }
  getAllProduct(){
    this.ProduitService.getAllProduct().subscribe((reas:any) =>{
      console.log("all products is : ",reas);
      this.Produit=reas;
    })
  }
}
