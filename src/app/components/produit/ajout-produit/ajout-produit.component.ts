import { FournisseurService } from 'src/app/_service/fournisseur.service';
import { CategoryService } from 'src/app/_service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProduitService } from 'src/app/_service/produit.service';

@Component({
  selector: 'app-ajout-produit',
  templateUrl: './ajout-produit.component.html',
  styleUrls: ['./ajout-produit.component.css']
})
export class AjoutProduitComponent implements OnInit {

  form:FormGroup;
  produit:any;
  categorie:any;
  fournisseur:any;
  // submitted:boolean=false;
    constructor(private fb:FormBuilder,private produitService:ProduitService,private router:Router,private CategoryService:CategoryService,private FournisseurService:FournisseurService) { }
  
    ngOnInit(): void {
      this.form=this.fb.group({
        reference:'',
        quantity:'',
        prix:'',
        color:'',
        id_categorie:'',
        id_fournisseur:'',
      })
      this.getAllprodct();
      this.getcat();
      this.getfour();
      
    }
    save(){
      console.log("here form",this.form.value);
     // if(this.testImage==true){
        //  console.log("here with image");
        //  let formData =new FormData();
   
        //  formData.append("username",this.form.value.reference)
        //  formData.append("prix",this.form.value.prix)
        //  formData.append("quantity",this.form.value.quantity)
        //  formData.append("color",this.form.value.color)
         this.produitService.saveproduit(this.form.value,this.form.value.id_categorie,this.form.value.id_fournisseur).subscribe((res:any) => {
         console.log("client ajouter est : ",res);
         this.produit=res;
         this.router.navigate(['home/ListeProduit']);
        //outhis.router.navigateByUrl("home/ListeProduit")
       })
      }
     

  getAllprodct(){

        this.produitService.getAllProduct().subscribe((reQs:any) =>{
          this.produit=reQs;
        }) 

  }
  getcat(){
    this.CategoryService.getALLcategory().subscribe((res:any)=>{
  console.log("category",res);

     this.categorie=res;
    })

 }
 getfour(){
this.FournisseurService.GetAllfournisseur().subscribe((ras:any)=>{
  console.log("fournisseur",ras);
  
 this.fournisseur=ras;
})
}
}  
