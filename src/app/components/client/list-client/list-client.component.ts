import { ClientService } from './../../../_service/client.service';
import { Component, OnInit } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {
Clients:any;
p:number=1;
term:string="";
  constructor(private clientService:ClientService) { }

  ngOnInit(): void {
    this.getAllClient();
  }
  getAllClient(){
    this.clientService.getAllClient().subscribe((res:any) =>{
      console.log("res",res);
      this.Clients=res;
    })
  }
  delete(id_client){
    // this.clientService.deleteClient(id_client).subscribe((res:any) =>{
    //   console.log("client supprimer");
    //   this.getAllClient();
    // })   
   
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.clientService.deleteClient(id_client).subscribe((res:any) =>{
             console.log("client supprimer",res);
            this.getAllClient();
        })
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  }
}
