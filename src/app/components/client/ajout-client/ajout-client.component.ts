import { ProduitService } from './../../../_service/produit.service';
import { ClientService } from './../../../_service/client.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-client',
  templateUrl: './ajout-client.component.html',
  styleUrls: ['./ajout-client.component.css']
})
export class AjoutClientComponent implements OnInit {
  fileToUpload: Array<File> = [];
  form : FormGroup;
  submitted:boolean=false;
  produits:any;
  constructor(private fb:FormBuilder,private clien:ClientService,private router:Router,private ProduitService:ProduitService ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username:['',Validators.required],
      email:['',Validators.required,Validators.email],
      date_de_naissance:['',Validators.required],
      password:['',Validators.required,Validators.minLength(6)],
      id_produit:''
      })
      this.getAllProduct();
  }
  save(){
    console.log("here form",this.form.value);

    if(this.form.invalid){
      console.log("this form invalid",this.form.invalid)
      this.submitted=true
      return;
    }

   // if(this.testImage==true){
       console.log("here with image");
       let formData =new FormData();
       formData.append("fichier",this.fileToUpload[0])
       formData.append("username",this.form.value.username)
       formData.append("email",this.form.value.email)
       formData.append("sexe",this.form.value.sexe)
       formData.append("date_de_naissance",this.form.value.date_de_naissance)
       formData.append("password",this.form.value.password)
       this.clien.saveClient(formData).subscribe((res:any) => {
       console.log("client ajouter est : ",res);
       
       this.router.navigate(['home/listClient']);//ou this.router.navigateByUrl("/list")
     })
    }
    handleFileInput(event: any) {
      this.fileToUpload = <Array<File>>event.target.files;
      console.log("file is ", this.fileToUpload);
     //  this.testImage=true
    }
    getAllProduct(){
      this.ProduitService.getAllProduct().subscribe((reas:any) =>{
        console.log("all products is : ",reas);
        this.produits=reas;
      })
    }
  

}
