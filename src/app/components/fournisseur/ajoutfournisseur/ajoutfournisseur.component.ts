import { FournisseurService } from 'src/app/_service/fournisseur.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajoutfournisseur',
  templateUrl: './ajoutfournisseur.component.html',
  styleUrls: ['./ajoutfournisseur.component.css']
})
export class AjoutfournisseurComponent implements OnInit {

  form:FormGroup;
  fournisseur:any;
  // submitted:boolean=false;
    constructor(private fb:FormBuilder,private FournisseurService:FournisseurService,private router:Router) { }
  
    ngOnInit(): void {
      this.form=this.fb.group({
        nom:'',
        company:'',
        date_de_naissance:''
      })
      this.getAllfournisseur();
    }
    save(){
      console.log("here form",this.form.value);
     // if(this.testImage==true){
         let formData =new FormData();
         formData.append("nom",this.form.value.nom)
         formData.append("company",this.form.value.company)
         formData.append("date_de_naissance",this.form.value.date_de_naissance)
         this.FournisseurService.savefournisseur(this.form.value).subscribe((res:any) => {
         console.log("client ajouter est : ",res);
         this.fournisseur=res;
         this.router.navigate(['home/listefournisseur']);
        //ou
         // this.router.navigateByUrl("listefournisseur")
       })
      }
     getAllfournisseur(){
       this.FournisseurService.GetAllfournisseur().subscribe((rees:any)=>{
         console.log("all fournisseur is : ",rees);
        
       this.fournisseur=rees;
     }
       ) }
}
  
