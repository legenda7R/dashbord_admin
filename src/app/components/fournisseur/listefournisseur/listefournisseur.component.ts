import { Component, OnInit } from '@angular/core';
import { FournisseurService } from 'src/app/_service/fournisseur.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listefournisseur',
  templateUrl: './listefournisseur.component.html',
  styleUrls: ['./listefournisseur.component.css']
})
export class ListefournisseurComponent implements OnInit {

  Fournisseur:any;

  constructor(private fournisseurService:FournisseurService) { }

  ngOnInit(): void {
    this.GetAllfournisseur();
  }
GetAllfournisseur(){
this.fournisseurService.GetAllfournisseur().subscribe((reas:any)=>{
  console.log("all fournisseur is : ",reas);
  
this.Fournisseur=reas;
})
}
delete(id_fournisseur){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.fournisseurService.deletefour(id_fournisseur).subscribe((res:any) =>{
           console.log("client supprimer",res);
          this.GetAllfournisseur();
      })
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
    }
  })
}
}