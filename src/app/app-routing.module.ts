import { ListCategoryComponent } from './components/category/list-category/list-category.component';
import { AjoutCategoryComponent } from './components/category/ajout-category/ajout-category.component';
// import { ListefournisseurComponent } from './components/fournisseurr/listefournisseur/listefournisseur.component';
// import { AjoutfournisseurComponent } from './components/fournisseurr/ajoutfournisseur/ajoutfournisseur.component';

import { LogoutGuard } from './_gardes/logout.guard';
import { LoginComponent } from './components/login/login.component';
import { ListeProduitComponent } from './components/produit/liste-produit/liste-produit.component';
import { AjoutClientComponent } from './components/client/ajout-client/ajout-client.component';
import { ListClientComponent } from './components/client/list-client/list-client.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_gardes/auth.guard';
import { AjoutProduitComponent } from './components/produit/ajout-produit/ajout-produit.component';
import { AjoutfournisseurComponent } from './components/fournisseur/ajoutfournisseur/ajoutfournisseur.component';
import { ListefournisseurComponent } from './components/fournisseur/listefournisseur/listefournisseur.component';

const routes: Routes = [
{path:"home",component:HomeComponent,canActivate:[AuthGuard],children:[
  {path:"listCategory",component:ListCategoryComponent},
  {path:"listClient",component:ListClientComponent},
  {path:"ajoutClient",component:AjoutClientComponent},
  {path:"ajoutCategory",component:AjoutCategoryComponent},
  
  {path:"ListeProduit",component:ListeProduitComponent},
   {path:"ajoutfournisseur",component:AjoutfournisseurComponent},
   {path:"listefournisseur",component:ListefournisseurComponent},
  {path:"AjoutProduit",component:AjoutProduitComponent},
  

]},  
//{path:"fournisseur",component:FournisseurComponent},
{path:"",component:LoginComponent,canActivate:[LogoutGuard]},
{path:"**",component:NotFoundComponent},

//
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
