import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsComponent } from './components/components.component';
import { HomeComponent } from './components/home/home.component';

import { FooterComponent } from './components/home/footer/footer.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { SidebarComponent } from './components/home/sidebar/sidebar.component';
import { LayoutComponent } from './components/home/layout/layout.component';
import { SkinsComponent } from './components/home/skins/skins.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AjoutClientComponent } from './components/client/ajout-client/ajout-client.component';
import { ListClientComponent } from './components/client/list-client/list-client.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListeProduitComponent } from './components/produit/liste-produit/liste-produit.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RecherchePipe } from './pipes/recherche.pipe';
import { RechercheEmailPipe } from './pipes/recherche-email.pipe';
import { LoginComponent } from './components/login/login.component';

import { AjoutProduitComponent } from './components/produit/ajout-produit/ajout-produit.component';
import { AjoutfournisseurComponent } from './components/fournisseur/ajoutfournisseur/ajoutfournisseur.component';
import { ListefournisseurComponent } from './components/fournisseur/listefournisseur/listefournisseur.component';
import { CategoryComponent } from './components/category/category.component';
import { AjoutCategoryComponent } from './components/category/ajout-category/ajout-category.component';
import { ListCategoryComponent } from './components/category/list-category/list-category.component';





@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    HomeComponent,
  
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    LayoutComponent,
    SkinsComponent,
    NotFoundComponent,
    AjoutClientComponent,
    ListClientComponent,
    ListeProduitComponent,
    RecherchePipe,
    RechercheEmailPipe,
    LoginComponent,
  
    AjoutClientComponent,
    AjoutProduitComponent,
    AjoutfournisseurComponent,
    ListefournisseurComponent,
    CategoryComponent,
    AjoutCategoryComponent,
    ListCategoryComponent,

    
  
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
