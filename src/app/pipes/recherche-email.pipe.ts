import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rechercheEmail'
})
export class RechercheEmailPipe implements PipeTransform {
  transform(value:any,term:any ): any {
    if(term==null){
      return value;
    }else{
      return value.filter((item:any)=>(item.email.includes(term)));
    }
  }

}
