import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recherche'
})
export class RecherchePipe implements PipeTransform {
  transform(value:any,term:any ): any {
    console.log("value",value);
    console.log("");
    
    
    if(term==null){
      return value;
    }else{
      return value.filter((item:any)=>(item.username.includes(term)));
    }
  }

}
