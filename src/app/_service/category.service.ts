import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }
  getALLcategory(){
    return this.http.get(`${environment.baseURL}/categorie/`)
  }
  saveCategorie(categorie:any){
    return this.http.post(`${environment.baseURL}/categorie/save`,categorie)
  }
  deletecat(id_categorie:any){
    return this.http.delete(`${environment.baseURL}/categorie/deleteById/${id_categorie}`)
    }
}
