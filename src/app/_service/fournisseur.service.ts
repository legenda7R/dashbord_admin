import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  constructor(private http:HttpClient) { }
  GetAllfournisseur(){
    return this.http.get(`${environment.baseURL}/fournisseur/`);
  }
  savefournisseur(Fournisseur:any){
    return this.http.post(`${environment.baseURL}/fournisseur/save`,Fournisseur)
  }
  deletefour(id_fournisseur:any){
    return this.http.delete(`${environment.baseURL}/fournisseur/deletebyid/${id_fournisseur}`)
  }
}
