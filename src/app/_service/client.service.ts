
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http:HttpClient) { }
  getAllClient(){
    return this.http.get(`${environment.baseURL}/client/`)
  }
  saveClient(Client:any){
    return this.http.post(`${environment.baseURL}/client/saveImg`,Client)
   }
   deleteClient(id_client:any){
    return this.http.delete(`${environment.baseURL}/client/deleteById/${id_client}`)
  }
  login(login:any){
    return this.http.post(`${environment.baseURL}/client/login`,login)
  }
}
