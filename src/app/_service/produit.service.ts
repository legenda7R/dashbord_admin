import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http:HttpClient) { }
  getAllProduct(){
    return this.http.get(`${environment.baseURL}/produit/`)
  }
  saveproduit(Produit:any,id_categorie:any,id_fournisseur:any){
    return this.http.post(`${environment.baseURL}/produit/save/${id_categorie}/${id_fournisseur}`,Produit)
   }
}
